/* Light YouTube Embeds by @labnol */
/* Web: http://labnol.org/?p=27941 */

document.addEventListener("DOMContentLoaded",
    function() {
        //loadIframes();
    });

function loadIframes() {
    var div, n,
        v = document.getElementsByClassName("youtube-player");
    for (n = 0; n < v.length; n++) {
        div = document.createElement("div");
        div.setAttribute("data-id", v[n].dataset.id);
        //div.innerHTML = labnolThumb(v[n].dataset.id);
        //div.onclick = labnolIframe;
        div.addEventListener('click', labnolIframe, false);
        v[n].appendChild(div);
    }
}

function labnolThumb(id) {
    var thumb = '<img src="https://i.ytimg.com/vi/ID/maxresdefault.jpg">';
    var play = '<div class="play"></div>';
    return thumb.replace("ID", id) + play;
}

function labnolIframe() {
    var parent = document.getElementById("video");
    var iframe = document.createElement("iframe");
    var embed = "https://www.youtube.com/embed/ID?autoplay=1";
    console.log(parent.getAttribute("data-id"));
    iframe.setAttribute("src", embed.replace("ID", parent.getAttribute("data-id")));
    iframe.setAttribute("frameborder", "0");
    iframe.setAttribute("allowfullscreen", "1");
    parent.appendChild(iframe); 
    //this.parentNode.replaceChild(iframe, this);
    $("#modal").fadeIn();
}
