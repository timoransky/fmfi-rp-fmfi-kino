$(document).ready(function() {

	function openModal(modalName) {
		$('.modal__box_container > *').hide();
		$('.modal__box_container > .' + modalName).fadeIn(function() {
			
		});
		$('.modal__box_container').fadeIn(function() {
			$('body').addClass("overflow_y_hide");
		});
		$('.modal__box_container').css("display", "flex");
	}

	function closeModal() {
		$('body').removeClass("overflow_y_hide");
		$(".modal__box_container").fadeOut();
	}

	function sendAjax(type) {
		var url = "php/" + type + ".php";
		var form = "#" + type + "Form";

		$(form + " input").removeClass('error');
		$(form + " span").text("");

		$.ajax({
	        url: url,
	        type: 'POST',
	        data: $(form).serialize(),
	        success: function(data) {
	        	console.log(data);
	            data = JSON.parse(data);
	        
	            for (var key in data['errors']) {
				    $(form + " #" + key).addClass("error");
				    $(form + " span." + key + "_error").text(data['errors'][key]);
				}
				if (data['info']) {
					if (data['info'] == 'success') {

						// just refresh the web for the time being...
						location.reload();

						if (type == 'login') {
							location.reload();
						} else {
							$(form)[0].reset();
							closeModal();
						}
						
					} else {
						console.log(data['info']);
					}
				}
	        }
	    })
	}

	function sendAjaxReaction(reviewId, reactionType, element) {
		var stored = localStorage['review_' + reviewId] || false;;
		console.log(stored);
		if (stored == false) {
			$.ajax({
		        url: 'php/sendAjaxReaction.php',
		        type: 'POST',
		        data: {'reviewId': reviewId, 'reactionType': reactionType},
		        success: function(data) {
		            data = JSON.parse(data);
					if (data['info']) {
						if (data['info'] == 'success') {
							localStorage['review_' + reviewId] = JSON.stringify(true);
							element.next().text(parseInt(element.next().text()) + 1);
						} else {
							console.log(data['info']);
						}
					}
		        }
		    })
		}	
	}

	if ($('#movie__details').length) {
		var movieTop = $('#movie__details').offset().top;
	}
	var headerHeight = $('.header').height();

	$(window).scroll(function() {
	    $('.header').toggleClass('fixed', $(window).scrollTop() > 30 || (document.documentElement && document.documentElement.scrollTop) > 30);

	    if ($('.movie__details').length) {
		    headerHeight = $('.header').height();

		    var sh = $(window).scrollTop(); 

		    if (sh >= movieTop - headerHeight) {
		    	$('.movie__details').css("position", "fixed");
		    	$('.movie__details').css("top", headerHeight);
		    } else {
		    	$('.movie__details').css("position", "initial");
		    }
		}	   
	});

	if ($(window).scrollTop() > 100) $('header').addClass('fixed');

	$('#loginButtonOpenModal, #reserveButtonOpenModalToLogin').click(function(e) {
		e.preventDefault();
		openModal('login_form_login');
	})

	$('.closeModal').click(function(e) {
		e.preventDefault();
		closeModal();
	})

	$('.switch_forms').click(function(e) {
		e.preventDefault();
		var elThis = $(this);
		elThis.parents( ".modal__box_inner_form" ).fadeOut(function(){
	        if (elThis.parents( ".modal__box_inner_form" ).next(".modal__box_inner_form").length) {
				elThis.parents( ".modal__box_inner_form" ).next(".modal__box_inner_form").fadeIn();
			} else {
				elThis.parents( ".modal__box_inner_form" ).prev(".modal__box_inner_form").fadeIn();
			}
	    });
	})

	$('#registerButtonSendAjax').click(function(e) {
		e.preventDefault();
		sendAjax('register');
	})

	$('#loginButtonSendAjax').click(function(e) {
		e.preventDefault();
		sendAjax('login');
	})

	$('#reviewButtonSendAjax').click(function(e) {
		e.preventDefault();
		sendAjax('review');
	})

	$('.openModalReview').click(function(e) {
		e.preventDefault();
		openModal('write__review');
	})

	$('#reserveButtonOpenModal').click(function(e) {
		e.preventDefault();
		openModal('seat__reservation');
	})

	$('#reserveButtonSendAjax').click(function(e) {
		e.preventDefault();
		sendAjax('reservation');
	})

	$('#addMovieOpenModal').click(function(e) {
		e.preventDefault();
		openModal('add__movie');
	})

	$('#addMovieButtonSendAjax').click(function(e) {
		e.preventDefault();
		sendAjax('addmovie');
	})

	$('.reaction').click(function(e) {
		e.preventDefault();
		console.log($(this).attr('data-reaction-type') + " of " + $(this).attr('data-reaction-review'));
		sendAjaxReaction($(this).attr('data-reaction-review'), $(this).attr('data-reaction-type'), $(this));
	})
/*
	$('.section__thumbs_slick').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		centerMode: true,
		arrow: true,
		autoplay: true,
		centerPadding: '50px',
		pauseOnHover: false,
		autoplaySpeed: 3000,
		speed: 1000
	});
*/
});