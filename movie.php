<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sk">
<head>
	<?php
		require "php/functions.php";
		if (isset($_GET["id"])) {
			$movieID = $_GET["id"];
			$movie = $imdb->LoadMovie($movieID, true);
		}
		require "php/_partials/_head.php";
	?>
</head>
<body>
	<?php
		require "php/_partials/_nav.php";
	?>

	<div class="slideshow section__hero" id="slideshow">
        <div class="section__hero_movie" id="movie_<?php echo $movie->id ?>">
        	<div class="play">
        		<a href="https://www.youtube.com/watch?v=<?php echo $movie->trailer ?>" data-fancybox>

					<svg class="video-overlay-play-button" viewBox="0 0 200 200" alt="Play video">
						<filter id="dropshadow" height="130%">
							<feGaussianBlur in="SourceAlpha" stdDeviation="20"/> <!-- stdDeviation is how much to blur -->
							<feOffset dx="0" dy="0" result="offsetblur"/> <!-- how much to offset -->
							<feMerge> 
								<feMergeNode/> <!-- this contains the offset blurred image -->
								<feMergeNode in="SourceGraphic"/> <!-- this contains the element that the filter is applied to -->
							</feMerge>
						</filter>
					    <circle cx="100" cy="100" r="90" fill="none" stroke-width="15" stroke="#000"/>
					    <polygon points="70, 55 70, 145 145, 100" fill="#000"/>
					</svg>
        		</a>
        	</div>
            <div class="movie__backdrop">
            	<img src="https://image.tmdb.org/t/p/w1280<?php echo $movie->backdrop ?>">
                <div class="movie__overlay"></div>
            </div>
            <div class="movie__title">
            	<h1 class="heading h1"><?php echo $movie->title ?></h1>
            </div>
            <div class="movie__details" id="movie__details">
                <div class="container">
                	<div class="flex">
                		<p><i class="fa fa-star" aria-hidden="true"></i>Priemerné hodnotenie: <?php echo $movie->rating['avg'] ?></p>
                		<p><i class="fa fa-caret-up" aria-hidden="true"></i>Maximálne hodnotenie: <?php echo $movie->rating['max'] ?></p>
                		<p><i class="fa fa-caret-down" aria-hidden="true"></i>Minimálne hodnotenie: <?php echo $movie->rating['min'] ?></p>
                	</div>
                	<?php
					if (isset($_SESSION['user'])) {
						?>
						<a href="" id="reserveButtonOpenModal" class="btn"><i class="fa fa-ticket" aria-hidden="true"></i>Rezervovať lístky</a>

						<?php
					} else {
						?>
						<a href="" id="reserveButtonOpenModalToLogin" class="btn"><i class="fa fa-ticket" aria-hidden="true"></i>Rezervovať lístky</a>

						<?php
					}
				?>
                </div>
            </div>
        </div>
	</div>

	<div class="section__box_shadow">
		<div class="section__movie">
			<div class="container">
				<div class="section__movie_poster">
					<img src="https://image.tmdb.org/t/p/w500/<?php echo $movie->poster ?>">
				</div>
				<div class="section__movie_details">
					<div class="row">
						<p class="text bold">Dátum vydania</p>
						<p class="text"><?php echo date('d. m. Y', $movie->date) ?></p>
					</div>
					<div class="row">
						<p class="text bold">Režisér</p>
						<p class="text"><?php echo $movie->director ?></p>
					</div>
					<div class="row">
						<p class="text bold">Obsadenie</p>
						<p class="text"><?php echo $movie->cast ?></p>
					</div>
					<!--
					<div class="row">
						<p class="text bold">Dĺžka</p>
						<p class="text">128 min.</p>
					</div>
					<div class="row">
						<p class="text bold">Jazyk</p>
						<p class="text">Anglicky</p>
					</div>
					<div class="row">
						<p class="text bold">Titulky</p>
						<p class="text">SK</p>
					</div>
					-->
					<div class="row">
						<p class="text bold">Žánre</p>
						<p class="text"><?php echo $movie->genres ?></p>
					</div>
				</div>
				<div class="section__movie_info">
					<h3 class="heading h3">O filme</h3>
					<p class="text"><?php echo $movie->overview ?></p>
				</div>
			</div>
		</div>

		<div class="section__reviews">
			<div class="section__reviews_top">
				<h3 class="heading h3">Recenzie</h3>
				<?php
					if (isset($_SESSION['user'])) {
						?>
						<a href="" class="btn openModalReview"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Napísať recenziu</a>
						<?php
					} 
				?>
			</div>
			<div class="container">

				<?php
				if ($movie->reviews) {
					foreach ($movie->reviews as $review) {
					?>
					<div class="section__reviews_review">
						<div class="review_header">
							<div class="review_header_left">
								<h4 class="heading h4"><?php echo $review['login_name'] ?></h4>
								<p class="review_header_date"><?php echo date('d. m. Y', strtotime($review['date'])) ?></p>
							</div>
							<div class="review_header_reactions">
								<div class="review_reaction">
									<a href="" class="reaction" data-reaction-review="<?php echo $review['id'] ?>" data-reaction-type="like"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
									<p><?php echo $review['likes'] ?></p>
								</div>
								<div class="review_reaction">
									<a href="" class="reaction" data-reaction-review="<?php echo $review['id'] ?>" data-reaction-type="dislike"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a>
									<p><?php echo $review['dislikes'] ?></p>
								</div>		
							</div>
							
						</div>
						<div class="review_body">
							<p><?php echo $review['text'] ?></p>
						</div>
					</div>
					<?php
					}
				} else {
				?>
				<p style="display: block;margin: 40px auto;">Nikto na tento film ešte nenapísal žiadnu recenziu. Buď prvý!</p>
				<?php
				}
				?>
			</div>
		</div>
	</div>
	
	<script>
		$( document ).ready(function() {
			if ($('.play').find('a')[0].href == 'https://www.youtube.com/watch?v=null') {
				$('.play').fadeOut();
			}
		});
	</script>
	<?php
		require "php/_partials/_footer.php"
	?>
</body>
</html>