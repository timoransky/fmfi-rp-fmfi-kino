<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="sk">
<head>
	<?php
		require "php/functions.php";
		require "php/_partials/_head.php";
	?>
</head>
<body>
	<script>
		$("#slideshow > .section__hero_movie").first().fadeIn(1000);
		
		setInterval(function() { 
			$('#slideshow > .section__hero_movie:first')
			    .fadeOut(1000)
			    .next()
			    .fadeIn(1000)
			    .css("display", "flex")
			    .end()
			    .appendTo('#slideshow');
		},  3000);
		
		$(".index").css("paddingTop", $(".section__hero_movie").height());
	</script>

	<?php
		require "php/_partials/_nav.php";
	?>

	<div class="slideshow section__hero" id="slideshow">
		<?php
			foreach ($imdb->LoadIndexMovies() as $movie) {
		?>

        <div class='section__hero_movie' id='movie_<?php echo $movie->id ?>'>
            <div class='movie__backdrop'><img src='https://image.tmdb.org/t/p/w1280/<?php echo $movie->backdrop ?>'>
                <div class='movie__overlay'></div>
            </div>
            <div class='movie__title'><a class="heading h1" href='movie.php?id=<?php echo $movie->id ?>' class='movie__link'><?php echo $movie->title ?></a></div>
            <div class="movie__details" id="movie__details">
                <div class="container">
                	<div class="flex">
                		<p><i class="fa fa-star" aria-hidden="true"></i>Priemerné hodnotenie: <?php echo $movie->rating['avg'] ?></p>
                		<p><i class="fa fa-caret-up" aria-hidden="true"></i>Maximálne hodnotenie: <?php echo $movie->rating['max'] ?></p>
                		<p><i class="fa fa-caret-down" aria-hidden="true"></i>Minimálne hodnotenie: <?php echo $movie->rating['min'] ?></p>
                	</div>
                	<a href="movie.php?id=<?php echo $movie->id ?>" class="btn"><i class="fa fa-info" aria-hidden="true"></i>Podrobnosti</a>
                </div>
            </div>
        </div>

		<?php
        	}
		?>
	</div>

	<div class="section__box_shadow section__movies">
		<div class="inner_container">
			<h2>Najnovšie filmy</h2>
			<div class="container">
			<?php
			foreach ($imdb->GetLatestMovies(5) as $movie) {
			?>
				<div class="section__movies_movie">
					<img src="https://image.tmdb.org/t/p/w500/<?php echo $movie->poster ?>">
					<div class="movie_link_container">
						<a class="movie_link btn" href="movie.php?id=<?php echo $movie->id ?>">Viac</a>
					</div>
				</div>
			<?php
			}
			?>
			</div>
		</div>
	</div>

	
	
	<!--
	<div class="charts">
		<h2>Najbližšie prehrávame</h2>
		<div class="chart" id="upcoming"></div>
	</div>
	-->

	<?php
		require "php/_partials/_footer.php"
	?>
</body>
</html>