<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="sk">
<head>
	<?php
		require "php/functions.php";
		require "php/_partials/_head.php";
	?>
</head>
<body>

	<?php
		require "php/_partials/_nav.php";
	?>

	<div class="section__greybox"></div>

	<div class="section__movies">
		<div class="container">
			<?php
			if (isset($_SESSION['admin'])) {
				if ($_SESSION['admin'] === true) {
			?>
					<div id="addMovieOpenModal" class="section__movies_movie add_movie">
						<i class="fa fa-plus fa-5x" aria-hidden="true"></i>
					</div>
			<?php
				}
			}

			$slideshowMovies = $imdb->LoadIndexMovies();
			$movies = $imdb->LoadAllMovies();
			usort($movies, "cmp_title");

			foreach ($movies as $movie) {
			?>
				<div class="section__movies_movie">
					<img src="https://image.tmdb.org/t/p/w500/<?php echo $movie->poster ?>">
					<div class="movie_link_container">
						<a class="movie_link btn" href="movie.php?id=<?php echo $movie->id ?>">Viac</a>
					</div>
					<?php
					if (isset($_SESSION['admin'])) {
						if ($_SESSION['admin'] === true) {
							if (in_array($movie, $slideshowMovies)) {
					?>
							<a class="slideshow_link" href="php/addmovietoslideshow.php?id=<?php echo $movie->dbid ?>&added=true"><i class="fa fa-check-circle fa-2x" aria-hidden="true"></i></a>
					<?php
							} else {
					?>
							<a class="slideshow_link" href="php/addmovietoslideshow.php?id=<?php echo $movie->dbid ?>&added=false"><i class="fa fa-circle-thin fa-2x" aria-hidden="true"></i></a>
					<?php
							}
					?>
							<a class="delete_link" href="php/deletemovie.php?id=<?php echo $movie->dbid ?>"><i class="fa fa-times-circle fa-2x" aria-hidden="true"></i></a>
					<?php
						}
					}
					?>
				</div>
			<?php
			}
			?>
		</div>
	</div>



	<?php
		require "php/_partials/_footer.php"
	?>
</body>
</html>