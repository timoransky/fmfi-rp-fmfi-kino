<?php
session_start();
require "_conf.php";

$msg = [];

if(isset($_POST)){
	$author = $_POST['reserve_user'];
	$movie = $_POST['reserve_movie'];

	$userIDQuery = "SELECT id FROM `users` WHERE `login_name` = '$author' ";
	$movieIDQuery = "SELECT id FROM `movies` WHERE `api_id` = '$movie' ";

	$result = $mysqli->query($userIDQuery);
	$userID = $result->fetch_array(MYSQLI_ASSOC)['id'];

	$result = $mysqli->query($movieIDQuery);
	$movieID = $result->fetch_array(MYSQLI_ASSOC)['id'];

	$seats = "";

	foreach ($_POST as $key => $value) {
		// NOTE: check for the right format of the regular expression 
		if (preg_match("/^seat*/", $key)) {
			$seats .= $key . ";";
		} 
	}

	if ($seats !== "") {
		$query = "INSERT INTO `reservations` (`id`, `movie_id`, `user_id`, `seats`) VALUES (NULL, '$movieID', '$userID', '$seats')";

		$result = $mysqli->query($query);

		if ($result) {
			// the message
			$mailBody = "First line of text\nSecond line of text";

			// use wordwrap() if lines are longer than 70 characters
			$mailBody = wordwrap($mailBody,70);

			// send email
			//mail("j.timoransky@gmail.com","Rezervácia lístkov",$mailBody);

			$msg['info'] = 'success';
			echo json_encode($msg);
			exit;
	    }
	}

	$msg['info'] = 'there was error, try again later please';
	echo json_encode($msg);
	exit;
}