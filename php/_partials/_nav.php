<?php 
	require '_modal.php';
?>

<header class="header">
	<div class="container">
		<?php
			if (isset($_SESSION['user'])) {
				?>
				<span class="btn_group">
					<a class="btn left login"><i class="fa fa-user" aria-hidden="true"></i><?php echo $_SESSION['user'] ?></a>
					<a href="php/logout.php" class="btn right logout"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
				</span>

				<?php
			} else {
				?>
				<a href="" id="loginButtonOpenModal" class="btn login"><i class="fa fa-sign-in" aria-hidden="true"></i>Prihlásiť sa</a>

				<?php
			}
		?>
		<nav class="header__nav">
			<ul>
				<li><a href="index.php">Domov</a></li>
				<li><a href="movies.php">Filmy</a></li>
				<li><a href="reviews.php">Recenzie</a></li>
			</ul>
		</nav>
	</div>
</header>