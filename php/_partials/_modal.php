<div class="modal__box_container">
	<div class="modal__box_inner add__movie">
		<div class="close closeModal"></div>
		<h3 class="heading h3">Pridať film</h3>
		<form id="addmovieForm" class="form" method="POST" action="">
			<label for="movie_name">Názov</label>
			<span class="movie_name_error"></span>
			<input id="movie_name" name="movie_name" type="text" required>
			<button id="addMovieButtonSendAjax" class="btn" name="add_movie_button">Pridať</button>
		</form>
	</div>
	<div class="modal__box_inner modal__box_inner_form login_form_login">
		<div class="close closeModal"></div>
		<h3 class="heading h3">Prihlásiť sa</h3>
		<form id="loginForm" class="form" method="POST" action="">
			<label for="login_name">Meno</label>
			<span class="login_name_error"></span>
			<input id="login_name" name="login_name" type="text" required>
			<label for="login_pass">Heslo</label>
			<span class="login_pass_error"></span>
			<input id="login_pass" name="login_pass" type="password" required>
			<button id="loginButtonSendAjax" class="btn" name="login_button">Prihlásiť</button>
		</form>
		<a href="" class="switch_forms">Registrovať sa</a>
	</div>
	<div class="modal__box_inner modal__box_inner_form login_form_register">
		<div class="close closeModal"></div>
		<h3 class="heading h3">Registrovať sa</h3>
		<form id="registerForm" class="form" method="POST" action="">
			<label for="register_name">Meno</label>
			<span class="register_name_error"></span>
			<input id="register_name" name="register_name" type="text" required>
			<label for="register_pass1">Heslo</label>
			<span class="register_pass1_error"></span>
			<input id="register_pass1" name="register_pass1" type="password" required>
			<label for="register_pass2">Heslo</label>
			<span class="register_pass2_error"></span>
			<input id="register_pass2" name="register_pass2" type="password" required>
			<button id="registerButtonSendAjax" class="btn" name="register_button">Registrovať</button>
		</form>
		<a href="" class="switch_forms">Prihlásiť sa</a>
	</div>
	<div class="modal__box_inner write__review">
		<div class="close closeModal"></div>
		<h3 class="heading h3">Napísať recenziu</h3>
		<form id="reviewForm" class="form" method="POST" action="">
			<label for="review_rating">Hodnotenie</label>
			<select name="review_rating" id="review_rating">
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
				<option value="5">5</option>
				<option value="6">6</option>
				<option value="7">7</option>
				<option value="8">8</option>
				<option value="9">9</option>
				<option value="10">10</option>
			</select>
			<label for="review_text">Text</label>
			<span class="review_text_error"></span>
			<textarea name="review_text" id="review_text" cols="30" rows="10"></textarea>
			<input type="hidden" name="review_user" value="<?php echo $_SESSION['user'] ?>">
			<input type="hidden" name="review_movie" value="<?php echo $movieID ?>">
			<button id="reviewButtonSendAjax" class="btn" name="review_button">Odoslať</button>
		</form>
	</div>
	<div class="modal__box_inner seat__reservation">
		<div class="close closeModal"></div>
		<h3 class="heading h3">Rezervujte si svoje miesta</h3>
		<form id="reservationForm" class="form" method="POST" action="">
			<div class="platno"></div>
			<div class="seats__container">
				<?php			
					if (!$movie) {
						# code...
					} else {
						foreach(range('a','f') as $r) {
							?>
							<div class="seats__container_row">
							<?php
								foreach(range(1, 6) as $c) {
								?>
								<div class="seats__container_seat">
									<input type="checkbox" id="seat_<?php echo $r.$c ?>" name="seat_<?php echo $r.$c ?>" <?php if (in_array("seat_$r$c", $movie->reservedSeats)) {echo "disabled";}  ?>><label for="seat_<?php echo $r.$c ?>"><?php echo $r.$c ?></label>
								</div>
								<?php
								}
							?>
							</div>
							<?php
						}
					}
				?>
			</div>
			<input type="hidden" name="reserve_user" value="<?php echo $_SESSION['user'] ?>">
			<input type="hidden" name="reserve_movie" value="<?php echo $movieID ?>">
			<button id="reserveButtonSendAjax" class="btn" name="reserve_button">Rezervovať</button>
		</form>
	</div>
</div>