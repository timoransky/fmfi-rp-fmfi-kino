<meta charset="UTF-8">
<title>FMFI Kino</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" href="gfx/css/main.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
<link rel="stylesheet" href="gfx/css/stars.css">
<script src="gfx/js/themoviedb.js"></script>
<script type="text/javascript" src="gfx/js/fastiframe.js"></script>

<script src="https://use.fontawesome.com/7dd1dbd1a9.js"></script>

<link rel="stylesheet" href="gfx/fancybox/jquery.fancybox.min.css" />
<script src="gfx/fancybox/jquery.fancybox.min.js"></script>

<link rel="stylesheet" type="text/css" href="slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="slick/slick-theme.css"/>

<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
<link rel="icon" href="favicon.ico" type="image/x-icon">
