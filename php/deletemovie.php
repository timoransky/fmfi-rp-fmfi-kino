<?php
session_start();
require "_conf.php";

if (!isset($_SESSION['admin']))
	exit;

if(isset($_GET['id'])){
  	if (!empty($_GET['id'])) {
  		$id = $_GET['id'];
		if (is_numeric($id)) {
			$query = "DELETE FROM `movies` WHERE `id` = $id";
			if ($mysqli->query($query)) {
				header("Location: ../movies.php");
			}
		}
	}
}