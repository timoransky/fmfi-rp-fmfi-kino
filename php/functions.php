<?php

require "_conf.php";


function cmp_title($a, $b) {
    return strcmp($a->title, $b->title);
}

/*
 * To isté, ako rovnako pomenovaná funkcia triedy 'IMDB', len trochu pozmenená... asi :D
 */
function LoadMultiCurl($data, $options = array()) {
    $curly = array();
    $result = array();
    $mh = curl_multi_init();

    foreach ($data as $id => $d) {
        $curly[$id] = curl_init();

        $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
        curl_setopt_array($curly[$id], array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_POSTFIELDS => "{}",
            CURLOPT_SSL_VERIFYPEER => false
        ));

        if (!empty($options)) {
            curl_setopt_array($curly[$id], $options);
        }

        curl_multi_add_handle($mh, $curly[$id]);
    }

    $running = null;
    do {
        curl_multi_exec($mh, $running);
    } while($running > 0);

    foreach($curly as $id => $c) {
        $result[$id] = curl_multi_getcontent($c);
        curl_multi_remove_handle($mh, $c);
    }

    curl_multi_close($mh);
    return $result;
}

class Movie {
    public $id, $title, $poster, $backdrop, $date, $overview, $genres, $rating;

    /*
     * Konštruktor triedy 'Movie'. Funguje na dvojúrovňovej báze. V prvej úrovni sa načítajú iba základné údaje zobrazované na úvodnej stránke.´V druhej úrovni sa do objektu načítajú aj detailnejšie informácie. 
     */
    function __construct($id, $title, $poster, $backdrop, $date = null, $overview = null, $genres = null) {
        $this->id = $id;
        $this->dbid = $this->getDBID();
        $this->title = $title;
        $this->poster = $poster;
        $this->backdrop = $backdrop;
        $this->rating = $this->getRating();
        
        if ($date != null) {
            $this->date = strtotime($date);
            $this->overview = $overview;
            if (is_array($genres) || is_object($genres)) {
                $this->genres = $this->toString($genres);
            }
            $this->trailer = $this->getTrailer();
            $this->cast = $this->toString($this->getCast());
            $this->director = $this->getDirector();
            $this->reviews = $this->getReviews();
            $this->reservedSeats = $this->getreservedSeats();   
        } 
    } 

    /*
     * Funkcia načíta pre daný film rating všetkých recenzií filmu. Následne vráti asociatívne pole s priemerným, maximálnym a minimálnym hodnotením. 
     */
    function getRating() {
        global $mysqli;
        $rating = [];
        $_tmp_rating = [];

        $avg = 0;
        $min = 0;
        $max = 0;

        $query = "SELECT rating from reviews, movies WHERE reviews.movie_id = movies.id AND reviews.movie_id = '$this->dbid'";
        if ($result = $mysqli->query($query)) {
            while ($row = $result->fetch_array(MYSQLI_ASSOC))
            {
                array_push($_tmp_rating, intval($row['rating']));
            }
            /* free result set */
            $result->close();
        }

        sort($_tmp_rating);

        if (count($_tmp_rating) >= 1) {
            $rating['avg'] = array_sum($_tmp_rating) / count($_tmp_rating);
            $rating['max'] = $_tmp_rating[count($_tmp_rating)-1];
            $rating['min'] = $_tmp_rating[0];
        } else {
            $rating['avg'] = 0;
            $rating['max'] = 0;
            $rating['min'] = 0;
        }

        return $rating;
    }

    /*
     * Funkcia vráti z DB id daného filmu v databáze, na základe API_ID stĺpca 
     */
    function getDBID() {
        global $mysqli;
        $dbid = [];

        $query = "SELECT `id` FROM `movies` WHERE `api_id` = '$this->id'";
        if ($result = $mysqli->query($query)) {
            while ($row = $result->fetch_array(MYSQLI_ASSOC))
            {
                $dbid = $row['id'];
            }
            /* free result set */
            $result->close();
        }

        return $dbid;
    }

    /*
     * Funkcia vráti pre daný film všetky doposiaľ rezervované miesta.
     */
    function getreservedSeats() {
        global $mysqli;
        $seats = [];

        $query = "SELECT `seats` FROM `reservations` WHERE `movie_id` = $this->dbid GROUP BY `seats`";
        if ($result = $mysqli->query($query)) {
            while ($row = $result->fetch_array(MYSQLI_ASSOC))
            {
                $tmpSeats = explode(';', $row["seats"]);
                foreach ($tmpSeats as $tmpSeat) {
                    if ($tmpSeat !== '') {
                        array_push($seats, $tmpSeat);
                    }
                }
            }
            /* free result set */
            $result->close();
        }

        return $seats;
    }

    /*
     * Funckia vráti pre daný film všetky užívatelské recenzie.
     */
    function getReviews() {
        global $mysqli;
        $reviews = [];
        $query = "SELECT reviews.id, reviews.date, reviews.text, reviews.likes, reviews.dislikes, reviews.rating, users.login_name FROM reviews INNER JOIN users ON reviews.author_id=users.id INNER JOIN movies ON reviews.movie_id=movies.id WHERE movies.api_id = $this->id";

        if ($result = $mysqli->query($query)) {
            while ($row = $result->fetch_array(MYSQLI_ASSOC))
            {
                array_push($reviews, $row);
            }
            /* free result set */
            $result->close();
        }
        return $reviews;
    }

    /*
     * Funckia vráti pre daný film meno režiseŕa.
     */
    function getDirector() {
        $director = '';
        $url = "https://api.themoviedb.org/3/movie/$this->id/credits?api_key=1ee33a0353991dcdfef2940347ceb402&language=sk-SK";
        $response = json_decode(LoadMultiCurl([$url])[0]);

        foreach ($response->{'crew'} as $crewMember)  {            
            if ($crewMember->{'job'} == 'Director') {
                $director = str_replace(" ", "&nbsp;", $crewMember->{'name'});
                return $director;
            }
        } 

        return $director; 
    }

    /*
     * Funckia vráti pre daný film prvé 4 mená hercov obsadenia.
     */
    function getCast() {
        $cast = [];
        $url = "https://api.themoviedb.org/3/movie/$this->id/credits?api_key=1ee33a0353991dcdfef2940347ceb402&language=sk-SK";
        $response = json_decode(LoadMultiCurl([$url])[0]);
        
        for ($i = 0; $i < 5; $i++) {
            $actor = $response->{'cast'}[$i]->{'name'};
            if (!in_array($actor, $cast)) {
                $actor = str_replace(" ", "&nbsp;", $actor);
                array_push($cast, $actor);
            }
        } 

        return $cast;  
    }

    /*
     * Funckia vráti pre daný film link na trailer na youtube.
     */
    function getTrailer() {
        $trailer = 'null';
        $url = "https://api.themoviedb.org/3/movie/$this->id/videos?api_key=1ee33a0353991dcdfef2940347ceb402&language=en-US";
        $response = json_decode(LoadMultiCurl([$url])[0]);
        
        foreach ($response->{'results'} as $element) {
            if ($element->{'type'} == 'Trailer') {
                $trailer = $element->{'key'};
                return $trailer;
            }
        } 

        return $trailer;
    }

    function toString($array) {
        $string = '';
        foreach ($array as $element) {
            $string .= $element . ', ';
        } 
        return substr($string, 0, strlen($string) - 2);
    }
}

class IMDB { 
    private $apiKey;
    private $urls = [];
    public $movies = [];

    /*
     * Konštruktor triedy 'IMDB', ktorý nastaví API klúč
     */
    function __construct($apiKey) {
        $this->apiKey = $apiKey;
    } 

    /*
     * Funkcia načíta a vráti API URL adresy pre jednotlivé filmy. V prípade, že ako input dostane ID len jedného filmu, vráti iba jednu adresu
     */
    private function __loadUrls($ids) {
        if (is_array($ids)) {
            $urls = [];
            foreach ($ids as $id) {
                array_push($urls, "https://api.themoviedb.org/3/movie/$id?language=sk-SK&api_key=$this->apiKey");
            }
            return $urls;
        } else {
            return "https://api.themoviedb.org/3/movie/$ids?language=sk-SK&api_key=$this->apiKey";
        }
    }

    /*
     * Funkcia najskôr načíta URL adresy pre jednotlivé ID filmov, ktoré dostane, následne nad nimi zavolá simultnánny cURL request a z dát, ktoré dostane poskladá pole objektov typu 'Movie'
     */
    private function __loadAndObjectifyData($_tmp_movies) {
        $movies = [];
        $data = $this->LoadMultiCurl($this->__loadUrls($_tmp_movies));

        foreach ($data as $movieData) {
            $movie = $this->LoadMovie('', false, $movieData);
            array_push($movies, $movie);
        }

        return $movies;
    }

    /*
     * Funkcia cez cURl requesty načíta a vráti dáta pre jednotlivé URLs, ktoré dostane ako input
     */
    private function LoadMultiCurl($data, $options = array()) {
        $curly = array();
        $result = array();
        $mh = curl_multi_init();

        foreach ($data as $id => $d) {
            $curly[$id] = curl_init();

            $url = (is_array($d) && !empty($d['url'])) ? $d['url'] : $d;
            curl_setopt_array($curly[$id], array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_POSTFIELDS => "{}",
                CURLOPT_SSL_VERIFYPEER => false
            ));

            if (!empty($options)) {
                curl_setopt_array($curly[$id], $options);
            }

            curl_multi_add_handle($mh, $curly[$id]);
        }

        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while($running > 0);

        foreach($curly as $id => $c) {
            $result[$id] = json_decode(curl_multi_getcontent($c));
            curl_multi_remove_handle($mh, $c);
        }

        curl_multi_close($mh);
        return $result;
    }
    
    /*
     * Funkcia načíta film... Neviem, čo viac k tomu, lebo sa to možno už v kóde ani nepoužíva a neviem či toto niekto vôbec číta. Úprimne dúfam, že áno a netrápim sa s tým nadarmo, zatiaľ čo som mal robiť landing web pre novú kampaň O2 Volnosť... Ahoj, čitateľ, pozdravujem ťa z minulosti a želám ti pekný deň :)
     */
    public function LoadMovie($movieID, $detail=false, $data=[]){
        
        if ($detail) {
            $movieData = $this->LoadMultiCurl([$this->__loadUrls($movieID)])[0];
        } else {
            $movieData = $data;
        }

        if ($detail === false) {
            $movie = new Movie($movieData->{'id'}, $movieData->{'title'}, $movieData->{'poster_path'}, $movieData->{'backdrop_path'});
        } else {
            $genres = [];

            foreach ($movieData->{'genres'} as $genre) {
                array_push($genres, $genre->{'name'});
            }

            $movie = new Movie($movieData->{'id'}, $movieData->{'title'}, $movieData->{'poster_path'}, $movieData->{'backdrop_path'}, $movieData->{'release_date'}, $movieData->{'overview'}, $genres);
        }
        return $movie;
    }

    /*
     * Funkcia načíta z DB najnovšie filmy, ktoré následne podsunie funkcii '__loadAndObjectifyData()', ktorá vráti pole objektov typu 'Movie'. Táto funckia následne dané pole vráti a v template sa dáta spracujú a zobrazia. Limit príde do funkcie ako input.
     */
    public function GetLatestMovies($maxLimit) {
        global $mysqli;
        $movieIDs = [];

        if ($result = $mysqli->query("SELECT * FROM movies ORDER BY `id` DESC LIMIT $maxLimit")) {
            while ($movie = $result->fetch_array(MYSQLI_ASSOC))
            {
                array_push($movieIDs, $movie["api_id"]);
            }
            $result->close();
        }

        return $this->__loadAndObjectifyData($movieIDs); 
    }

    /*
     * Funkcia načíta z DB filmy zobrazené na hlavnej stránke, ktoré následne podsunie funkcii '__loadAndObjectifyData()', ktorá vráti pole objektov typu 'Movie'. Táto funckia následne dané pole vráti a v template sa dáta spracujú a zobrazia
     */
    function LoadIndexMovies(){
        global $mysqli;
        $movieIDs = [];

        if ($result = $mysqli->query("SELECT * FROM movies WHERE carousel = 1 AND carousel_order >= 0 ORDER BY carousel_order ASC")) {
            while ($movie = $result->fetch_array(MYSQLI_ASSOC))
            {
                array_push($movieIDs, $movie["api_id"]);
            }
            $result->close();
        }

        return $this->__loadAndObjectifyData($movieIDs);       
    }

    /*
     * Funkcia načíta z DB filmy všetky filmy, ktoré následne podsunie funkcii '__loadAndObjectifyData()', ktorá vráti pole objektov typu 'Movie'. Táto funckia následne dané pole vráti a v template sa dáta spracujú a zobrazia
     */
    function LoadAllMovies(){
        global $mysqli;
        $movieIDs = [];

        if ($result = $mysqli->query("SELECT * FROM movies ORDER BY id ASC")) {
            while ($movie = $result->fetch_array(MYSQLI_ASSOC))
            {
                array_push($movieIDs, $movie["api_id"]);
            }
            $result->close();
        }

        return $this->__loadAndObjectifyData($movieIDs); 
    }
} 

$imdb = new IMDB(API_KEY);

if (isset($_POST['login_name']) && !isset($_SESSION['user'])) {
    //$_SESSION['user'] = $_POST['login_name'];
}
