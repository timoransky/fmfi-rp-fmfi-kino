<?php
session_start();
require "_conf.php";

$msg = [];

if(isset($_POST)){
  	if (empty($_POST['login_name'])) {
		$msg['errors']['login_name'] = 'Vyplňte pole';
	} elseif (strlen($_POST['login_name']) < 5) {
		$msg['errors']['login_name'] = 'Meno musí mať aspoň 5 znakov';
	}
	if (empty($_POST['login_pass'])) {
		$msg['errors']['login_pass'] = 'Vyplňte pole';
	} elseif (strlen($_POST['login_pass']) < 4) {
		$msg['errors']['login_pass'] = 'Heslo musí mať aspoň 4 znaky';
	} 
 
 	if (array_key_exists('errors', $msg)) {
 		if(count($msg['errors']) > 0){
			//This is for ajax requests:
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&  strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			    echo json_encode($msg);
			    exit;
			}
		}	
	} else {
		$login_name = $mysqli->real_escape_string(strip_tags($_POST['login_name']));
		$login_pass = sha1($mysqli->real_escape_string(strip_tags($_POST['login_pass'])));

		$query = "SELECT * FROM `users` WHERE `login_name` = '$login_name' AND `login_password` = '$login_pass'";

		$result = $mysqli->query($query);

		if ($result) {
			if ($result->num_rows > 0) {
				$admin = $result->fetch_array(MYSQLI_ASSOC)['admin'];

				if (intval($admin) === 1) {
					$_SESSION['admin'] = true;
				}
				$_SESSION['user'] = $login_name;
				$msg['info'] = 'success';
				echo json_encode($msg);
				exit;
			} else {
				$msg['info'] = 'Zadaný používateľ nebol nájdený';
				$msg['errors']['login_name'] = 'Zlé meno alebo heslo';
				$msg['errors']['login_pass'] = 'Zlé meno alebo heslo';
				echo json_encode($msg);
		    	exit;
			}
	    } else {
	    	$msg['info'] = 'Nastala chyba pri prihlásení';
			echo json_encode($msg);
	    	exit;
	    }
   }
}