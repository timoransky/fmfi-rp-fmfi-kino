<?php
session_start();
require "_conf.php";
require "diacriticsFree.php";

if (!isset($_SESSION['admin']))
	exit;

function SearchForMovie($searchQuery) {
    $curl = curl_init();

    $searchQuery = remove_accents($searchQuery);

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.themoviedb.org/3/search/movie?api_key=".API_KEY."&query=$searchQuery",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_POSTFIELDS => "{}",
        CURLOPT_SSL_VERIFYPEER => false
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        return "cURL Error #:" . $err;
    } else {
        return $response;
    }   
}

$msg = [];

if(isset($_POST)){

  	if (empty($_POST['movie_name'])) {
		$msg['errors']['movie_name'] = 'Vyplňte pole';
	}
 
 	if (array_key_exists('errors', $msg)) {
 		if(count($msg['errors']) > 0){
			//This is for ajax requests:
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&  strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			    echo json_encode($msg);
			    exit;
			}
		}	
	} else {

		$searchQuery = str_replace(' ', '+', $_POST['movie_name']);

		$movie =  json_decode(SearchForMovie(remove_accents($searchQuery)));

		if ($movie) {
			$movie = $movie->{'results'}[0]->{'id'};

			if ($movie) {
				$query = "SELECT * FROM `movies` WHERE `api_id` = '$movie'";

				$result = $mysqli->query($query);

				if ($result->num_rows == 0) {
					$query = "INSERT INTO `movies` (`id`, `api_id`, `carousel`, `carousel_order`) VALUES (NULL, '$movie', '0', NULL)";

					if ($mysqli->query($query)) {
						$msg['info'] = 'success';
						echo json_encode($msg);
						exit;
				    } else {
				    	$msg['info'] = 'there was error, try again later please';
						echo json_encode($msg);
				    	exit;
				    }
				} else {
					$msg['errors']['movie_name'] = 'Film už je pridaný';
					echo json_encode($msg);
				    exit;
				}
			}
		}
   }
}