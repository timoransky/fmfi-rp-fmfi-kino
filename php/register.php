<?php
session_start();
require "_conf.php";

$msg = [];

if(isset($_POST)){
  	if (empty($_POST['register_name'])) {
		$msg['errors']['register_name'] = 'Vyplňte pole';
	} elseif (strlen($_POST['register_name']) < 5) {
		$msg['errors']['register_name'] = 'Meno musí mať aspoň 5 znakov';
	}
	if (empty($_POST['register_pass1'])) {
		$msg['errors']['register_pass1'] = 'Vyplňte pole';
	} elseif (strlen($_POST['register_pass1']) < 8) {
		$msg['errors']['register_pass1'] = 'Heslo musí mať aspoň 8 znakov';
	} 
	if (empty($_POST['register_pass2'])) {
		$msg['errors']['register_pass2'] = 'Vyplňte pole';
	} elseif (strlen($_POST['register_pass2']) < 8) {
		$msg['errors']['register_pass2'] = 'Heslo musí mať aspoň 8 znakov';
	} elseif (strlen($_POST['register_pass1']) >= 8 && $_POST['register_pass1'] != $_POST['register_pass2']) {
		$msg['errors']['register_pass1'] = $msg['errors']['register_pass2'] = 'Heslá sa musia zhodovať';
	}
 
 	if (array_key_exists('errors', $msg)) {
 		if(count($msg['errors']) > 0){
			//This is for ajax requests:
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&  strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			    echo json_encode($msg);
			    exit;
			}
		}	
	} else {
		$register_name = $mysqli->real_escape_string(strip_tags($_POST['register_name']));
		$register_pass = sha1($mysqli->real_escape_string(strip_tags($_POST['register_pass1'])));

		$query = "INSERT INTO `users` (`id`, `login_name`, `login_password`, `name`, `admin`) VALUES (NULL, '$register_name', '$register_pass', NULL, '0')";

		if ($mysqli->query($query)) {
			$_SESSION['user'] = $register_name;
			$msg['info'] = 'success';
			echo json_encode($msg);
			exit;
	    } else {
	    	$msg['info'] = 'there was error, try again later please';
			echo json_encode($msg);
	    	exit;
	    }
   }
}