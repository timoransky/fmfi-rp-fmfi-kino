<?php
session_start();
require "_conf.php";

$msg = [];

if(isset($_POST)){
  	if (empty($_POST['review_text'])) {
		$msg['errors']['review_text'] = 'Vyplňte pole';
	}
 
 	if (array_key_exists('errors', $msg)) {
 		if(count($msg['errors']) > 0){
			//This is for ajax requests:
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&  strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			    echo json_encode($msg);
			    exit;
			}
		}	
	} else {
		$reviewText = $mysqli->real_escape_string(strip_tags($_POST['review_text']));
		$author = $_POST['review_user'];
		$movie = $_POST['review_movie'];
		$rating = $_POST['review_rating'];

		$userIDQuery = "SELECT id FROM `users` WHERE `login_name` = '$author' ";
		$movieIDQuery = "SELECT id FROM `movies` WHERE `api_id` = '$movie' ";

		$result = $mysqli->query($userIDQuery);
		$userID = $result->fetch_array(MYSQLI_ASSOC)['id'];

		$result = $mysqli->query($movieIDQuery);
		$movieID = $result->fetch_array(MYSQLI_ASSOC)['id'];

		$query = "SELECT id FROM `reviews` WHERE `movie_id` = '$movieID' AND `author_id` = '$userID'";

		$result = $mysqli->query($query);

		if ($result) {
			if ($result->num_rows == 0) {
				$query = "INSERT INTO `reviews` (`id`, `movie_id`, `author_id`, `date`, `text`, `likes`, `dislikes`, `rating`) VALUES (NULL, '$movieID', '$userID', CURRENT_TIMESTAMP, '$reviewText', '0', '0', '$rating')";

				if ($mysqli->query($query)) {
					$msg['info'] = 'success';
					echo json_encode($msg);
					exit;
			    }
			} else {
				$msg['info'] = 'review_duplicite';
				echo json_encode($msg);
				exit;
			}
		}

    	$msg['info'] = 'there was error, try again later please';
		echo json_encode($msg);
    	exit;

   }
}