<?php
session_start();
require "_conf.php";

$msg = [];

if(isset($_POST)){
	$reviewID = $_POST['reviewId'];
	$reaction = $_POST['reactionType'];

	if ($reaction == "like") {
		$query = "UPDATE `reviews` SET `likes` = `likes` + 1 WHERE `id` = $reviewID";
	} elseif ($reaction == "dislike") {
		$query = "UPDATE `reviews` SET `dislikes` = `dislikes` + 1 WHERE `id` = $reviewID";
	} else {
		$msg['info'] = 'not supported reaction type';
		echo json_encode($msg);
		exit;
	}

	$result = $mysqli->query($query);

	if ($result) {
		$msg['info'] = "success";
		echo json_encode($msg);
		exit;
	} else {
		$msg['info'] = 'error';
		echo json_encode($msg);
		exit;
	}

	$msg['info'] = 'there was error, try again later please';
	echo json_encode($msg);
	exit;
}