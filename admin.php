<?php
	session_start();
?>
<!DOCTYPE html>
<html lang="sk">
<head>
	<?php
		require "php/functions.php";
		$imdb->LoadAllMovies();
		require "php/_partials/_head.php";
	?>
</head>
<body>

	<?php
		require "php/_partials/_nav.php";
	?>

	<div class="section__greybox"></div>

	<div class="section__movieslist">
		<div class="container">
			<?php
				if (isset($_SESSION['admin'])) {
					if ($_SESSION['admin'] === true) {
						echo "Ste prihlasney ako administrator";
						foreach ($imdb->movies as $movie) {
						?>
							<div class="section__movieslist_movie">
								<div class="movie_link_container">
									<a class="movie_link btn" href="movie.php?id=<?php echo $movie->id ?>" class="btn">Viac</a>
								</div>
							</div>
						<?php
						}
					}
				} else {
					echo "Nemate prava administratora";
				}
			?>
		</div>
	</div>



	<?php
		require "php/_partials/_footer.php"
	?>
</body>
</html>