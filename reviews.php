<?php
session_start();
?>
<!DOCTYPE html>
<html lang="sk">
<head>
	<?php
		require "php/functions.php";
		require "php/_partials/_head.php";
	?>
</head>
<body>
	<?php
		require "php/_partials/_nav.php";
	?>


	<div class="section__greybox"></div>

	<div class="section__box_shadow">
	<div class="section__reviews">
		<?php
		foreach ($imdb->LoadAllMovies() as $movie) {
			if (count($movie->getReviews()) > 0) {
				?>
					<div class="section__reviews_top">
						<h3 class="heading h3"><a href="movie.php?id=<?php echo $movie->id ?>"><?php echo $movie->title ?></a></h3>
						<a href="movie.php?id=<?php echo $movie->id ?>" class="btn"><i class="fa fa-info" aria-hidden="true"></i>Podrobnosti</a>
					</div>
					<div class="container">
				<?php
				foreach ($movie->getReviews() as $review) {
					?>
						<div class="section__reviews_review">
							<div class="review_header">
								<div class="review_header_left">
									<h4 class="heading h4"><?php echo $review['login_name'] ?></h4>
									<p class="review_header_date"><?php echo date('d. m. Y', strtotime($review['date'])) ?></p>
								</div>
								<div class="review_rating_container">
									<div class="review_rating">
										<p><i class="fa fa-star" aria-hidden="true"></i> <span class="bold"><?php echo $review['rating'] ?></span>/10</p>
									</div>
									<div class="review_header_reactions">
										<div class="review_reaction">
											<a class="reaction" data-reaction-type="like" data-reaction-review="<?php echo $review['id'] ?>" href=""><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></a>
											<p><?php echo $review['likes'] ?></p>
										</div>
										<div class="review_reaction">
											<a class="reaction" data-reaction-type="dislike" data-reaction-review="<?php echo $review['id'] ?>" href=""><i class="fa fa-thumbs-o-down" aria-hidden="true"></i></a>
											<p><?php echo $review['dislikes'] ?></p>
										</div>		
									</div>
								</div>
								
							</div>
							<div class="review_body">
								<p><?php echo $review['text'] ?></p>
							</div>
						</div>
					<?php
				}
				?>
					</div>
				<?php
			}
		}
		?>
	</div>
	</div>

	<?php
		require "php/_partials/_footer.php"
	?>
</body>
</html>